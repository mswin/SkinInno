#pragma once



typedef VOID ( _stdcall * SkinEventCallBack)( LPCWSTR lpszCtrlName, INT nEventID);


BOOL _stdcall SkinLoad( LPCWSTR lpszSkin, SkinEventCallBack pCallBack);

