#include "stdafx.h"

#include <msapi/EnumRegEx.h>
#include <msapi/msapp.h>
#include <msapi/mspath.h>
#include <util/registry.h>


using namespace msdk;

struct ClickOnceInfo
{
	CString DisplayName;
	CString ShortcutFileName;
	CString ShortcutFolderName;
	CString ShortcutSuiteName;

	//程序所在目录
	CString ProgPath;
	CString DeskTopLink;
	CString ProgramFolder;
	CString UninstallKey;
};

BOOL IsMyProg( HKEY hSKey, LPCTSTR lpszKey, LPCTSTR lpszPordName)
{
	BOOL MyProg = FALSE;
	std::function<BOOL(HKEY, LPCTSTR,LPBYTE,DWORD, DWORD)> IsMyProgFunc = [&MyProg,lpszPordName](HKEY hKey, LPCTSTR lpszName, LPBYTE lpData, DWORD dwDataLen, DWORD dwType) -> BOOL
	{
		if ( _wcsicmp(lpszName, L"DisplayName") == 0 && _wcsicmp(lpszPordName, (WCHAR*)lpData) == 0)
			MyProg = TRUE;
		return TRUE;
	};


	msapi::CEnumRegEx(hSKey).EnumValue(lpszKey, IsMyProgFunc);
	return MyProg;
}

BOOL GetProgInfo( HKEY hSKey, LPCTSTR lpszKey , ClickOnceInfo& info)
{
	std::function<BOOL(HKEY, LPCTSTR, LPBYTE, DWORD, DWORD)> GetInfo = [&info](HKEY hKey, LPCTSTR lpszName, LPBYTE lpData, DWORD dwDataLen, DWORD dwType) -> BOOL
	{
		if ( _wcsicmp(lpszName, L"DisplayName") == 0)
		{
			info.DisplayName = (WCHAR*)lpData;
		}

		if (_wcsicmp(lpszName, L"ShortcutFileName") == 0)
		{
			info.ShortcutFileName = (WCHAR*)lpData;
		}

		if (_wcsicmp(lpszName, L"ShortcutFolderName") == 0)
		{
			info.ShortcutFolderName = (WCHAR*)lpData;
		}

		if (_wcsicmp(lpszName, L"ShortcutSuiteName") == 0)
		{
			info.ShortcutSuiteName = (WCHAR*)lpData;
		}
		return TRUE;
	};

	return msapi::CEnumRegEx(hSKey).EnumValue(lpszKey, GetInfo);
}

BOOL GetClickOnceInfo( LPCTSTR ProdName, ClickOnceInfo& info )
{
	BOOL bFind = FALSE;
	std::function<BOOL(HKEY, LPCTSTR)> EnumKeys = [&](HKEY hKey, LPCTSTR lpszKey)->BOOL
	{
		if ( IsMyProg(hKey, lpszKey,ProdName))
		{
			bFind = TRUE;
			info.UninstallKey = L"Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\";
			info.UninstallKey.Append(lpszKey);
			GetProgInfo(hKey, lpszKey, info);
			return FALSE;	//找到了就不要再找了
		}
		return TRUE;
	};


	msapi::CEnumRegEx(HKEY_CURRENT_USER).EnumKey(L"Software\\Microsoft\\Windows\\CurrentVersion\\Uninstall", EnumKeys);

	if ( bFind )
	{
		SHGetSpecialFolderPath(NULL, info.ProgPath.GetBufferSetLength(MAX_PATH), CSIDL_LOCAL_APPDATA, FALSE);
		info.ProgPath.ReleaseBuffer();
		if ( msapi::GetMicroVersion() < msapi::MicroVersion_Vista)
			info.ProgPath.Append(L"\\..\\Apps\\2.0\\");
		else
			info.ProgPath.Append(L"\\Apps\\2.0\\");
		
		OutputDebugString(info.ProgPath);

		SHGetSpecialFolderPath(NULL, info.DeskTopLink.GetBufferSetLength(MAX_PATH), CSIDL_DESKTOP, FALSE);
		info.DeskTopLink.ReleaseBuffer();
		info.DeskTopLink.AppendFormat(L"\\%s.appref-ms", info.ShortcutFileName);

		SHGetSpecialFolderPath(NULL, info.ProgramFolder.GetBufferSetLength(MAX_PATH), CSIDL_PROGRAMS, FALSE);
		info.ProgramFolder.ReleaseBuffer();
		info.ProgramFolder.AppendFormat(L"\\%s", info.ShortcutFolderName);
	}

	return bFind;
}

extern"C"
{
	bool OC_IsProgExist( LPCWSTR ProgName)
	{
		ClickOnceInfo Info;
		return BOOL2bool( GetClickOnceInfo(ProgName, Info));
	}

	bool OC_RemoveProg( LPCWSTR PorgName )
	{
		ClickOnceInfo Info;
		if ( GetClickOnceInfo(PorgName, Info) )
		{
			for ( DWORD dwIndex = 0; dwIndex < 10 ; dwIndex++)
			{
				if ( PathFileExists( Info.ProgPath ))
				{
					msapi::ShellDeleteFile(Info.ProgPath);
					msapi::DeletePath(Info.ProgPath, TRUE, FALSE, TRUE);
				}
				else
					break;

				if ( PathFileExists(Info.ProgPath))
					Sleep(1000);
			}
			
			msapi::DeleteFileEx( Info.ProgPath);
			msapi::DeleteFileEx( Info.DeskTopLink );
			msapi::DeleteFileEx( Info.ProgramFolder );
			CRegistry::DeleteKey(HKEY_CURRENT_USER, Info.UninstallKey);
			return true;
		}

		return false;
	}
};
