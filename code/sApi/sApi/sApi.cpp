// sApi.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "sApi.h"
#include <string>
#include <userguid/userguid.h>
#include <msapi/msapp.h>
#include <msapi/mswinapi.h>
#include "EnumAntivirus.h"
#include "util/registry.h"
using namespace msdk;


extern"C"
{
	int GetUserGuid(WCHAR* szGuid, int nBuffer)
	{
		msapi::CApp::GetUserGuid(szGuid,(DWORD)MAX_PATH);
		return _tcslen(szGuid);
	}

	int GetDiskSerial( WCHAR* szDisk, int nBuffer)
	{
		msapi::CApp::GetDiskSerial(szDisk,(DWORD)MAX_PATH);
		return _tcslen(szDisk);
	}
	
	int GetAnt(WCHAR* szGuid, int nBuffer)
	{
		ZeroMemory( szGuid, nBuffer*sizeof(WCHAR));
		std::wstring sAnt = msapi::CEnumAntivirus().Enum();
		_tcsncpy( szGuid, sAnt.c_str(), nBuffer);
		return sAnt.length();
	}
	
	int  IsWindowsX64()
	{
		return msapi::IsWindowsX64();
	}

	int  GetMicroVersion()
	{
		return msapi::GetMicroVersion();
	}

	INT TerminateProcessAsName( LPCWSTR lpszProcName)
	{
		DWORD dwNumber = msapi::TerminateProcessAsName(lpszProcName);

		TCHAR szError[MAX_PATH] = { 0 };
		msapi::GetLastErrorText( szError, MAX_PATH);

		CString strLog;
		strLog.Format(L"TerminateProcessAsName(%s)  %d %s", lpszProcName,dwNumber, szError);
		OutputDebugString( strLog) ;
		return dwNumber;
	}

	bool ProcIsExist(LPCWSTR lpszProcName)
	{
		 DWORD dwProcId = msapi::GetProcessIdAsName( lpszProcName);
		 return dwProcId != -1;
	}


	INT SetDefProgram(LPCTSTR lpszPord, LPCTSTR lpszProg, LPCTSTR lpszExt, LPCTSTR lpszDes, LPCTSTR lpszParme , LPCTSTR lpszIco)
	{
		CRegistry().SetString(HKEY_CURRENT_USER, CString(_T("Software\\Classes\\")) + CString(lpszPord) + CString(lpszExt), _T(""), lpszDes);
		CRegistry().SetString(HKEY_CURRENT_USER, _T("Software\\Classes\\") +CString(lpszPord) + CString(lpszExt) +CString(_T("\\DefaultIcon")), _T(""), lpszIco);

		CString strCmd; strCmd.Format(_T("\"%s\" %s \"%%1\""), lpszProg, lpszParme);
		CRegistry().SetString(HKEY_CURRENT_USER, _T("Software\\Classes\\") +CString(lpszPord) + CString(lpszExt) +CString(_T("\\shell\\open\\command")), _T(""), strCmd);

		CRegistry().SetString(HKEY_CURRENT_USER, CString(_T("Software\\")) + CString(lpszExt), _T(""), CString(lpszPord) + CString(lpszExt));
		CRegistry().SetString(HKEY_CURRENT_USER, CString(_T("Software\\Classes\\"))+ CString(lpszExt), _T(""), CString(lpszPord) + CString(lpszExt));


		CString strLog;
		strLog.Format(L"SetDefProgram(%s,%s,%s,%s,%s,%s)",  lpszPord,  lpszProg,  lpszExt,  lpszDes,  lpszParme ,  lpszIco);
		OutputDebugString( strLog) ;


		return 0;
	}
};




