
#define IDPROOT ExtractFilePath(__PATHFILENAME__)

[Files]
Source: "{#IDPROOT}\sapi.dll";DestDir: {app}; Flags: ignoreversion;

[Code]
function _GetUserGuid(sGuid: String; nBufSize: Integer):Integer; external 'GetUserGuid@{tmp}/sapi.dll cdecl delayload' ;
function _GetAnt(sGuid: String; nBufSize: Integer):Integer; external 'GetAnt@{tmp}/sapi.dll cdecl delayload';
function _GetDiskSerial(sGuid: String; nBufSize: Integer):Integer; external 'GetDiskSerial@{tmp}/sapi.dll cdecl delayload' ;
function  IsWindowsX64():Integer; external 'IsWindowsX64@{tmp}/sapi.dll cdecl delayload';
function  GetMicroVersion(): Integer; external 'GetMicroVersion@{tmp}/sapi.dll cdecl delayload';

function  OC_RemoveProg(ProgName: String):Boolean; external 'OC_RemoveProg@{tmp}/sapi.dll cdecl delayload';
function  OC_IsProgExist(ProgName: String): Boolean; external 'OC_IsProgExist@{tmp}/sapi.dll cdecl delayload';

function  TerminateProcessAsName(ProgName: String): Integer; external 'TerminateProcessAsName@{tmp}/sapi.dll cdecl delayload';
function  ProcIsExist(ProgName: String): Boolean; external 'ProcIsExist@{tmp}/sapi.dll cdecl delayload';
function  SetDefProgram(lpszPord:String; lpszProg: String; lpszExt: String; lpszDes: String; lpszParme: String ; lpszIco: String):Integer; external 'SetDefProgram@{tmp}/sapi.dll cdecl delayload';

function GetUserGuid():String;
var sGuid: String;
	strLen:Integer;
begin
    SetLength(sGuid, 256);
	strLen := _GetUserGuid(sGuid,256)
	Result := Copy(sGuid, 1 , strLen);
end;

function GetDiskSerial():String;
var sGuid: String;
	strLen:Integer;
begin
    SetLength(sGuid, 256);
	strLen := _GetDiskSerial(sGuid,256)
	Result := Copy(sGuid, 1 , strLen);
end;

function GetAnt():String;
var sGuid: String;
	strLen:Integer;
begin
    SetLength(sGuid, 256);
	strLen := _GetAnt(sGuid,256)
	Result := Copy(sGuid, 1 , strLen);
end;


function HttpGET( URl: String ) : Boolean;
var WinHttpReq: Variant;
begin
	WinHttpReq := CreateOleObject('WinHttp.WinHttpRequest.5.1');
	WinHttpReq.Open('GET', URl, False);
	WinHttpReq.Send('');
	if WinHttpReq.Status <> 200 then
	begin
		Log('HTTP Error: ' + IntToStr(WinHttpReq.Status) + ' ' + WinHttpReq.StatusText);
		Result := False;
	end
    else
	begin
		Log('HTTP Response: ' + WinHttpReq.ResponseText);
		Result := True;
	end;
end;


procedure Explode(var Dest: TArrayOfString; Text: String; Separator: String);
var
  i, p: Integer;
begin
  i := 0;
  repeat
    SetArrayLength(Dest, i+1);
    p := Pos(Separator,Text);
    if p > 0 then begin
      Dest[i] := Copy(Text, 1, p-1);
      Text := Copy(Text, p + Length(Separator), Length(Text));
      i := i + 1;
    end else begin
      Dest[i] := Text;
      Text := '';
    end;
  until Length(Text)=0;
end;

const wbemFlagForwardOnly = $00000020;